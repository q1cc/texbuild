texbuild
========

work-in-progress server for building latex documents, triggered by github webhooks.

Setup
-----

- install [golang](http://golang.org), debian pkg `golang`
- install latex distribution and necessary packages
- generate an ssh key: `ssh-keygen` 
- in github repo:
    - add signature for ssh key as deploy key (see screenshot #1)
    - add webhook: (see screenshot #2)  
      url: `http://<server>/hook/`  
      content type: `application/json`  
      secret: think of something  
      events: just push
- start server:  
  `go run serve.go -secret <the secret you defined>`
- add portforward to your server, standard port is **8000**
- push to the max!
- visit `http://<server>/` to view your pdfs

![deploy key screenshot](http://i.imgur.com/70vhOYC.png)
![webhook screenshot](http://i.imgur.com/D6KZkDV.png)


Tips
----

Use [ngrok](https://ngrok.com/) for developing on your machine.
