package main

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

const DirPerm = os.ModeDir | 0755

type Server struct {
	port       int
	outDir     string
	gitDir     string
	secret     string
	outHandler http.Handler
}

type PushPayload struct {
	Head    string          `json:"head"`
	Ref     string          `json:"ref"`
	After   string          `json:"after"`
	Before  string          `json:"before"`
	Size    int             `json:"size"`
	Commits []CommitPayload `json:"commits"`
	Repo    RepoPayload     `json:"repository"`
}

type RepoPayload struct {
	Name        string      `json:"name"`
	URL         string      `json:"url"`
	Description string      `json:"description"`
	Owner       UserPayload `json:"owner"`
	Private     bool        `json:"private"`
}

type CommitPayload struct {
	ID        string      `json:"id"`
	Message   string      `json:"message"`
	Timestamp time.Time   `json:"timestamp"`
	Author    UserPayload `json:"author"`
	URL       string      `json:"url"`
	Distinct  bool        `json:"distinct"`
	Modified  []string    `json:"modified"`
	Added     []string    `json:"added"`
	Removed   []string    `json:"removed"`
}

type UserPayload struct {
	Name     string `json:"name"`
	Username string `json:"username"`
	EMail    string `json:"email"`
}

func main() {
	var s Server
	flag.IntVar(&s.port, "port", 8000, "listen port")
	flag.StringVar(&s.outDir, "out", "build/", "pdf output dir")
	flag.StringVar(&s.gitDir, "git", "git/", "git checkout dir")
	flag.StringVar(&s.secret, "secret", "", "webhook secret")
	flag.Parse()
	if s.secret == "" {
		panic("secret is required. try --help.")
	}

	s.Init()
	s.Serve()
}

func (s *Server) Init() {
	check(os.MkdirAll(s.outDir, DirPerm))
	check(os.MkdirAll(s.gitDir, DirPerm))

	http.HandleFunc("/", s.HandleOut)
	http.HandleFunc("/hook/", s.HandleHook)
	s.outHandler = http.FileServer(http.Dir(s.outDir))
}

func (s *Server) Serve() {
	addr := ":" + strconv.Itoa(s.port)
	fmt.Println("serving on", addr)
	http.ListenAndServe(addr, nil)
}

func (s *Server) HandleIndex(w http.ResponseWriter, r *http.Request) {
	s.HandleOut(w, r)
}

func (s *Server) HandleHook(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Incoming hook")
	fmt.Println("url:", r.URL)

	if event, ok := r.Header["X-Github-Event"]; !ok || len(event) != 1 || event[0] != "push" {
		s.printHTTPErr("not a valid event: "+fmt.Sprint(event), nil, w, http.StatusBadRequest)
		return
	}

	var body []byte
	var err error
	if signature, ok := r.Header["X-Hub-Signature"]; !ok {
		s.printHTTPErr("no signature!", nil, w, http.StatusUnauthorized)
		return
	} else if len(signature) != 1 {
		s.printHTTPErr("too many signatures", nil, w, http.StatusUnauthorized)
		return
	} else if !strings.HasPrefix(signature[0], "sha1=") {
		s.printHTTPErr("not allowed: "+strings.Split(signature[0], "=")[0], nil, w, http.StatusBadRequest)
		return
	} else {
		body, err = ioutil.ReadAll(r.Body)
		if err != nil {
			s.printHTTPErr("error reading body", err, w, http.StatusBadRequest)
			return
		}
		mac := hmac.New(sha1.New, []byte(s.secret))
		mac.Write(body)
		expectedMAC := mac.Sum(nil)
		expectedStr := hex.EncodeToString(expectedMAC)
		sign := strings.Split(signature[0], "=")[1]
		if expectedStr != sign {
			s.printHTTPErr("invalid signature", nil, w, http.StatusUnauthorized)
			fmt.Println("expected", expectedStr, "- got", sign)
			return
		}
	}

	var payload PushPayload
	err = json.Unmarshal(body, &payload)
	if err != nil {
		s.printHTTPErr("error parsing input:", err, w, http.StatusBadRequest)
		return
	}
	//fmt.Printf(`%+v`, payload)

	freshFolder := path.Join(s.gitDir, payload.Repo.Owner.Name)
	folder := path.Join(freshFolder, payload.Repo.Name)
	_, err = os.Stat(folder)
	fresh := false
	if os.IsNotExist(err) {
		fresh = true
	} else if err != nil {
		s.printHTTPErr("folder error", err, w, http.StatusInternalServerError)
		return
	}

	if fresh {
		err := os.MkdirAll(freshFolder, DirPerm)
		if err != nil {
			s.printHTTPErr("error mkdir: "+freshFolder, err, w, http.StatusInternalServerError)
			return
		}
		sshUrl := "git@github.com:" + payload.Repo.Owner.Name + "/" + payload.Repo.Name
		out, err := cmd(freshFolder, "git", "clone", "--no-checkout", sshUrl, payload.Repo.Name)
		if err != nil {
			s.printHTTPErr("error cloning: "+out, err, w, http.StatusInternalServerError)
			return
		}
		fmt.Println(out)
	} else {
		out, err := cmd(folder, "git", "fetch")
		if err != nil {
			s.printHTTPErr("error fetching: "+out, err, w, http.StatusInternalServerError)
			return
		}
		fmt.Println(out)
	}
	out, err := cmd(folder, "git", "checkout", "--quiet", payload.After)
	if err != nil {
		s.printHTTPErr("error checking out "+payload.After+": "+out, err,
			w, http.StatusInternalServerError)
		return
	}
	fmt.Println(out)

	files, err := ioutil.ReadDir(folder)
	if err != nil {
		s.printHTTPErr("error reading files: "+folder, err, w, http.StatusInternalServerError)
		return
	}
	for _, file := range files {
		if !file.IsDir() && strings.HasSuffix(file.Name(), ".tex") {
			out, err := s.pdflatex(folder, file.Name())
			if err != nil {
				s.printHTTPErr("error texing "+file.Name()+": "+out, err,
					w, http.StatusInternalServerError)
			}
		}
	}

	// copy pdfs
	pdfFolder := path.Join(s.outDir, payload.Repo.Owner.Name, payload.Repo.Name)
	err = os.MkdirAll(pdfFolder, DirPerm)
	if err != nil {
		s.printHTTPErr("error mkdir: "+pdfFolder, err, w, http.StatusInternalServerError)
		return
	}
	files, err = ioutil.ReadDir(folder)
	if err != nil {
		s.printHTTPErr("error reading files: "+folder, err, w, http.StatusInternalServerError)
		return
	}
	for _, file := range files {
		if !file.IsDir() && strings.HasSuffix(file.Name(), ".pdf") {
			fmt.Println("Produced", pdfFolder, file.Name())
			fmt.Fprintln(w, "Produced", file.Name())
			os.Rename(path.Join(folder, file.Name()), path.Join(pdfFolder, file.Name()))
		}
	}

	// reset working dir
	out, err = cmd(folder, "git", "reset", "--hard")
	if err != nil {
		s.printHTTPErr("error resetting git checkout", err,
			w, http.StatusInternalServerError)
		return
	}
	fmt.Println(out)
	out, err = cmd(folder, "git", "clean", "-df")
	if err != nil {
		s.printHTTPErr("error resetting git checkout", err,
			w, http.StatusInternalServerError)
		return
	}
	fmt.Println(out)
}

func (s *Server) pdflatex(dir string, file string) (string, error) {
	var buf string
	// latex to the rescue
	out, err := cmd(dir, "pdflatex", file)
	buf += out
	if err != nil {
		return out, err
	}

	out, err = cmd(dir, "bibtex", strings.Replace(file, ".tex", ".aux", -1))
	buf += out
	if err != nil {
		return out, err
	}
	fmt.Println(out)
	out, err = cmd(dir, "pdflatex", file)
	buf += out
	if err != nil {
		return out, err
	}

	out, err = cmd(dir, "pdflatex", file)
	buf += out

	return out, err
}

func cmd(dir string, cmds ...string) (string, error) {
	fmt.Println(cmds)
	cmd := exec.Command(cmds[0], cmds[1:]...)
	cmd.Dir = dir
	var out bytes.Buffer
	cmd.Stdout = &out
	cmd.Stderr = &out
	err := cmd.Run()
	return out.String(), err
}

func (s *Server) printHTTPErr(msg string, err error, w http.ResponseWriter, status int) {
	w.WriteHeader(status)
	fmt.Println(msg, err)
	fmt.Fprintln(w, msg, err)
}

func (s *Server) HandleOut(w http.ResponseWriter, r *http.Request) {
	url := r.URL
	urlPath := strings.Replace(url.String(), "..", "", -1)
	if strings.HasSuffix(urlPath, "/") {
		// list
		files := make(map[string]os.FileInfo)
		err := filepath.Walk(s.outDir, func(name string, info os.FileInfo, err error) error {
			if strings.HasPrefix(name, s.outDir) {
				name = name[len(s.outDir):]
			}
			files[name] = info
			return nil
		})
		if err != nil {
			fmt.Fprintln(w, err)
			return
		}
		s.head(w, urlPath)
		fmt.Fprint(w, `<h2>`, urlPath, `</h2>`)
		fmt.Fprintln(w, `<table><thead><td>path</td><td>date</td><td>size</td></thead>`)
		for name, file := range files {
			if file.IsDir() {
				continue
			}
			fmt.Fprint(w, `<tr><td><a href="`, name, `">`, name, `</a></td><td>`,
				file.ModTime(), `</td><td>`, file.Size(), `</td></tr>`)
		}
		fmt.Fprintln(w, `</table>`)
		s.foot(w)
	} else {
		s.outHandler.ServeHTTP(w, r)
	}
}

func (s *Server) head(w http.ResponseWriter, title string) {
	fmt.Fprint(w, `<html><head><title>`, title, `</title>
<style>
table thead td {
	border-bottom: 1px solid black;
}
table td {
	padding: 0.2em 1em;
}
</style>
		</head><body>`)
}
func (s *Server) foot(w http.ResponseWriter) {
	fmt.Fprint(w, `</body></html>`)
}

func check(err error) {
	if err != nil {
		panic(err)
	}
}
